package com.example.demo.controller;

import com.example.demo.model.Cuenta;

import org.springframework.ui.Model;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;

@Controller
public class TestController {

  // @RequestMapping("/")
  // public String start() {
	//   return "index";	
  // }

  // @GetMapping("/form")
  // public String form() {
	//   return "form";	
  // }

  // @PostMapping("/form")
  // public String profile() {
	//   return "form";	
  // }

  @RequestMapping("/")
  public String start(Model model) {
    model.addAttribute("cuenta", new Cuenta());
	  return "index";	
  }

  @PostMapping("/save-cuenta")
  public String start(@ModelAttribute Cuenta cuenta) {
	  return "save";	
  }
}
